package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;

import it.unibo.oop.lab.mvcio.Controller;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     */
	private final JFrame frame = new JFrame();
	
	public SimpleGUIWithFileChooser(final Controller ctrl) {
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		final int sw = (int) screen.getWidth();
		final int sh = (int) screen.getHeight();
		frame.setSize(sw / 2, sh / 2);
		
		frame.setLocationByPlatform(true);
		
		final JPanel canvas = new JPanel();
		canvas.setLayout(new BorderLayout());
		
		final JPanel canvas2 = new JPanel();
		
		final JButton browse = new JButton("Browse...");
		final JTextArea textArea = new JTextArea();
		
		final JTextField filepath = new JTextField(ctrl.getCurrentFilePath());
		filepath.setEditable(false);
		final JButton save = new JButton("Save"); 
		
		
		canvas2.setLayout(new BorderLayout());
		
		canvas2.add(filepath, BorderLayout.CENTER);
		canvas2.add(browse, BorderLayout.LINE_END);
		canvas.add(canvas2, BorderLayout.NORTH);
		canvas.add(textArea, BorderLayout.CENTER);
		canvas.add(save, BorderLayout.SOUTH);
		
		frame.setContentPane(canvas);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		save.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent event) {
				try {
					ctrl.save(textArea.getText());
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "An error occurred", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		browse.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent event) {
				final JFileChooser choice = new JFileChooser("Choose where to save...");
				choice.setSelectedFile(ctrl.getCurrentFile());
				
				final int result = choice.showSaveDialog(frame);
				switch (result) {
				case JFileChooser.APPROVE_OPTION:
					final File newDest = choice.getSelectedFile();
					ctrl.setDestination(newDest);
					textArea.setText(newDest.getPath());
					break;
				case JFileChooser.CANCEL_OPTION:
					break;
				default:
					JOptionPane.showMessageDialog(frame, result, "Mehhh", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		frame.pack();
		frame.validate();
		
	}
	
	public static void main(final String... args) {
		final SimpleGUIWithFileChooser gui = new SimpleGUIWithFileChooser(new Controller());
		gui.display();
	}
	
	private void display() {
		frame.setVisible(true);
	}
	
    /* 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

}
